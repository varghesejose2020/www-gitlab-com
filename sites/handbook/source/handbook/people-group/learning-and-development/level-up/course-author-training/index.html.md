---
layout: handbook-page-toc
title: Level Up
---

## On this page
{:.no_toc .hidden-md .hidden-lg}

- TOC
{:toc .hidden-md .hidden-lg}

# Level Up Course Author 101 Training
<!-- blank line -->
This handbook page is used to document the Level Up Course Author Training. The Learning & Development team use a [handbook first](/people-group/learning-and-development/interactive-learning/) approach to interactive learning, so the content in this page can also be found in the Level Up course, but is preserved here as a Single Source of Truth (SSOT).

The Course Author Training is designed for team members who would like to create their own learning content in Level Up. Once you've completed the course, let the Learning & Development team know and we'll uplift your permissions and allocate you a number of content items to give you access to the system.

## Switching between admin and learner views

When your permissions are uplifted, you will be granted access to the admin side of the platform. This video shows you how to switch between the Learner and Admin views, so that you can create content, but also still navigate the platform as a regular learner.

<figure class="video_container">
  <iframe width="560" height="315" src="https://youtu.be/W6eSaOrJIHk" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>
</figure>

## Course Design Overview - Sections, Lessons, and Pages

Courses in Level Up are split into Sections, Lessons and Pages. This video gives an overview of the differences between each of them, how they are displayed to learners, and how to deploy them as part of your learning design.

<figure class="video_container">
  <iframe width="560" height="315" src="https://youtu.be/u9iT5vcy-SA" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>
</figure>

### Course Design Overview - Adding Pages

The fundamental building blocks of courses in Level Up are Pages. This video describes a little about what Pages are, and how they work.

<figure class="video_container">
  <iframe width="560" height="315" src="https://youtu.be/8Atj2ypcstY" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>
</figure>

### Course Design Overview - iFrames

Given our handbook first principle for building learning content, iFrames will likely be a common feature of many Learning courses. This video demonstrates how to build iFrames in LevelUp.

<figure class="video_container">
  <iframe width="560" height="315" src="https://youtu.be/gGQbH3WkMRw" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>
</figure>

### Course Design Overview - Quizzes and Tests

Creating knowledge checks are a key part of knowledge retention and a useful tool to employ in your learning courses. Watch this video to learn about how to set one up and the nuances of the different types available.

<figure class="video_container">
  <iframe width="560" height="315" src="https://youtu.be/Biul9dstZC8" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>
</figure>

### Tips and Tricks

<figure class="video_container">
  <iframe width="560" height="315" src="https://youtu.be/6v9wRBPbwDQ" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>
</figure>

### Preparing content for release

There are some specific actions to take to ensure your conrent is ready to be released:

- Adding a thumbnail
- Adding a description
- Setting completion criteria
- Adding certifications

This video reviews the steps required.

<figure class="video_container">
  <iframe width="560" height="315" src="https://youtu.be/D0QGDtk-8tI" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>
</figure>

### Releasing content

For releasing content to internal audiences, two steps are needed - publishing and adding the internal tag. This video covers these steps.

<figure class="video_container">
  <iframe width="560" height="315" src="https://youtu.be/E1kjsaz8j2Y" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>
</figure>
